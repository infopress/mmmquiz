<div class="pager">
	<?php include 'logo.php' ?>
	<div class="text-holder">
		<div class="container">
			<h1 class="title-ajax"><?= $page['title'] ?></h1>
		</div>
	</div>
	<div class="attached-bottom">
		<div class="container ajax-items-content">
			<form class="select-form select-items-container" action="#">
				<ul class="radio-list">
					<?php foreach ($page['questions'] as $key => $question): ?>
						<li>
							<label for="radio<?= $key + 1 ?>">
								<input id="radio<?= $key + 1 ?>" type="radio" name="p<?= $pageNum ?>" value="<?= $key + 1 ?>">
								<span class="fake-input"></span>
								<span class="fake-label">
									<span class="icon">
										<span class="main"><img src="pub/quiz/images/<?= $question['img'][0] ?>" alt="image description" width="30" height="33"></span>
										<span class="hover"><img src="pub/quiz/images/<?= $question['img'][1] ?>" alt="image description" width="30" height="33"></span>
									</span>
									<span class="text"><?= $question['text'] ?></span>
								</span>
							</label>
						</li>
					<?php endforeach; ?>
				</ul>
			</form>
		</div>
	</div>
</div>
