<div class="pager pager4">
	<?php include 'logo.php' ?>
	<div class="page-area">
		<div class="text-holder">
			<div class="holder">
				<div class="container">
					<h1 class="title-ajax"><?= $page['title'] ?></h1>
				</div>
			</div>
		</div>
		<div class="attached-bottom">
			<div class="container ajax-items-content">
				<form class="select-form2 select-items-container" action="#">
					<fieldset>
						<ul class="radio-list2">
							<?php foreach ($page['questions'] as $key => $question): ?>
								<li>
									<label for="radio<?= $key + 1 ?>">
										<input id="radio<?= $key + 1 ?>" type="radio" name="p<?= $pageNum ?>" value="<?= $key + 1 ?>">
										<span class="fake-input"></span>
										<span class="fake-label"><?= $question['text'] ?></span>
									</label>
								</li>
							<?php endforeach; ?>
						</ul>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
