<div class="page"
<?php if (!empty($gtagPagePath)): ?>
		 data-gtag-page-path="<?= $gtagPagePath ?>"
	 <?php endif ?>
	 data-percent="95"
	 >
    <section class="section2">
        <div class="pager pager5">
            <div class="container">
                <h1>Let’s create your profile and see your results:</h1>
				<form method="POST" data-action="/profile" action="https://mindmoodmore.activehosted.com/proc.php" id="results" class="detail-form" novalidate>
					<div class="error-list"<?php if (!\App::flash('resultsFormHasError')): ?> hidden<?php endif; ?>>
						<div class="error"><span class="icon"><img src="/pub/quiz/images/icon-error.svg" alt="image description"></span><span class="message">All fields are required, please fill them</span></div>
						<div class="error"><span class="icon"><img src="/pub/quiz/images/icon-error.svg" alt="image description"></span><span class="message">Please accept all agreements before continueing</span></div>
					</div>
					<input type="hidden" name="u" value="1" />
					<input type="hidden" name="f" value="1" />
					<input type="hidden" name="s" />
					<input type="hidden" name="c" value="0" />
					<input type="hidden" name="m" value="0" />
					<input type="hidden" name="act" value="sub" />
					<input type="hidden" name="v" value="2" />
					<input type="hidden" name="quiz_data">
					<div class="row">
                        <div class="col">
                            <label for="firstname">First name:</label>
                            <input name="firstname" type="text" id="firstname">
                        </div>
                        <div class="col">
                            <label for="lastname">Last name:</label>
                            <input name="lastname" type="text" id="lastname">
                        </div>
                        <div class="col">
                            <label for="date">Date of Birth:</label>
                            <div class="date-holder">
                                <input name="datebirth" type="text" class="datepicker" id="date">
                                <a href="#ui-datepicker-div" class="icon"><img src="/pub/quiz/images/icon-calendar.svg" alt="image description"></a>
                            </div>
                        </div>
                        <div class="col">
                            <label for="gender">Gender:</label>
                            <div class="fake-select">
                                <select name="gender" id="gender">
                                    <option value="m">Male</option>
                                    <option value="f">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <label for="email">Email:</label>
                            <input name="email" type="email" id="email">
                        </div>
                        <div class="col">
                            <label for="password">Password:</label>
                            <input name="password" type="password" id="password">
                        </div>
                    </div>
                    <ul class="check-list">
                        <li>
                            <label for="agree1">
                                <input id="agree1" type="checkbox">
                                <span class="fake-input"></span>
                                <span class="fake-label">I agree to Mind Mood More’s <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></span>
                            </label>
                        </li>
                        <li>
                            <label for="agree2">
                                <input id="agree2" type="checkbox">
                                <span class="fake-input"></span>
                                <span class="fake-label">I acknowledge and agree to the processing of personal data relating to my health, that I elect to supply to Mind Mood More, Inc. when using the Services, for purposes of providing the Mind Mood More Services as described in the Privacy Policy</span>
                            </label>
                        </li>
                        <li>
                            <label for="agree3">
                                <input id="agree3" type="checkbox">
                                <span class="fake-input"></span>
                                <span class="fake-label">I acknowledge and agree that personal data relating to my health that I elect to supply to Mind Mood More, Inc. will be transferred to the United States, which may not have data protection laws similar to the EEA, for purposes of providing the Mind Mood More services as described in the Privacy Policy</span>
                            </label>
                        </li>
                    </ul>
					<div class="btn-holder">
                        <input type="submit" value="Get Results">
                    </div>
				</form>
            </div>
        </div>
    </section>
</div>