<div class="page"
<?php if (!empty($gtagPagePath)): ?>
		 data-gtag-page-path="<?= $gtagPagePath ?>"
	 <?php endif ?>
	 data-percent="<?= $percent ?>"
	 data-url-hash="<?= $urlHash ?>"
	 <?php if (!empty($nextUrlHash)): ?>
		 data-next-url-hash="<?= $nextUrlHash ?>"
	 <?php elseif (!empty($nextUrl)): ?>
		 data-next-url="<?= $nextUrl ?>"
	 <?php endif ?>>
	<section class="section">
		<?= $this->render(sprintf('quiz/inc/section_v%d.php', $page['page_type'])) ?>
	</section>
</div>