<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?= $title ?></title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700%7cMuli:400,600,700%7cOpen+Sans:400,700%7cRoboto+Slab:300,400,700" rel="stylesheet">
		<link media="all" rel="stylesheet" href="/pub/quiz/css/main.css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" defer></script>
		<script>window.jQuery || document.write('<script src="/pub/quiz/js/jquery-3.2.1.min.js" defer><\/script>')</script>
		<script src="/pub/quiz/js/jquery.validate.min.js" defer></script>
		<script>
			pathInfo = {
				base: 'pub/quiz/',
				css: 'css/'
			};
		</script>
		<script src="/pub/quiz/js/jquery.main.js" defer></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-45824178-8"></script>
        <script>
			window.dataLayer = window.dataLayer || [];
			function gtag() {
				dataLayer.push(arguments);
			}
			function gtagInit(path) {
				gtag('js', new Date());
				gtag('config', 'UA-45824178-8', {page_path: path});
			}
        </script>
    </head>
	<body>
		<div id="wrapper">
			<div class="ajax-switch-container">
				<div class="progressbar">
					<div class="line-holder">
						<div class="line" style="width: 0;">
							<span class="progress-value">0%</span>
						</div>
					</div>
				</div>
				<div class="ajax-container" data-hash-page="<?= \App::flash('page') ?: (empty($inc) ? '#page-1' : '') ?>">
					<?php if (!empty($inc)) echo $this->render($inc) ?>
				</div>
			</div>
		</div>
	</body>
</html>