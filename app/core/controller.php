<?php

namespace Core;

class Controller {

	public static function alertFlash($name = NULL, $value = NULL) {
		$f3 = \Base::instance();
		$data = (array) \App::flash('alert');
		if ($name) {
			if ($value == NULL) {
				return $data;
			}
			if (!array_key_exists($name, $data)) {
				$data[$name] = array();
			}
			foreach ((array) $value as $val) {
				array_push($data[$name], $val);
			}
			\App::flash('alert', $data);
		} elseif ($data) {
			$f3->set('alert', $data);
		}
	}

	public function beforeroute($f3) {
		$reflector = new \ReflectionClass(get_called_class());
		$f3->set('UI', dirname($reflector->getFileName()) . '/views/;' . $f3->get('UI'));
	}

}
