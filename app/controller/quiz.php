<?php

namespace Controller;

class Quiz extends Main {

	protected $_questions = array();

	public function beforeroute($f3) {
		parent::beforeroute($f3);
		$this->_questions = include 'data/questions.php';
	}

	public function index($f3) {
		\App::response([], 'quiz/layout.php');
	}

	public function page($f3, $params) {
		$pageNum = empty($params['page']) ? 1 : (int) $params['page'];
		$page = $this->_getPage($pageNum);
		if (!$page) {
			$pageNum = 1;
			$page = $this->_getPage($pageNum);
		}
		//
		if ($page) {
			$resp = [
				'gtagPagePath' => '/quiz/page/' . $pageNum,
				'percent' => $this->_getPercent($page, $pageNum),
				'urlHash' => $this->_getUrlHash($pageNum),
				'pageNum' => $pageNum,
				'page' => $page,
			];
			if ($this->_getPage($pageNum + 1)) {
				$resp['nextUrlHash'] = $this->_getUrlHash($pageNum + 1);
			} else {
				$resp['nextUrl'] = $f3->alias('profile');
			}
			\App::response($resp, 'quiz/inc/page.php');
		}
	}

	private function _getPage($pageNum) {
		$key = $pageNum - 1;
		if (/* $key > -1 and */ isset($this->_questions[$key])) {
			return $this->_questions[$key];
		}
	}

	private function _getPercent(Array $page, $pageNum) {
		return (int) (isset($page['percent']) ? $page['percent'] : ($pageNum / count($this->_questions) * 90));
	}

	private function _getUrlHash($pageNum) {
		return sprintf('page-%d', $pageNum);
	}

}
