<?php

namespace Controller;

class Results extends Main {

	public function index($f3) {
		\App::response([
			'gtagPagePath' => '/quiz/results',
			'inc' => 'results/results.php',
				], 'results/layout.php');
	}

	public function thanks($f3) {
		\App::response([
			'gtagPagePath' => '/quiz/results',
			'inc' => 'results/thanks.php',
				], 'results/layout.php');
	}

}
