<?php

namespace Controller;

class Profile extends Quiz {

	public function index($f3) {
		$this->_getQuizData();
		\App::response([
			'gtagPagePath' => '/quiz/optin',
			'inc' => 'quiz/inc/profile.php',
				], 'quiz/layout.php');
	}

	public function exec($f3) {

		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$datebirth = date_create($_POST['datebirth']);
		$gender = $_POST['gender'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		//$quizData = $_POST['quiz_data'];
		$quizData = $this->_getQuizData();

		//$error = TRUE;

		if (!empty($error)) {
			\App::flash('page', '#profile');
			\App::flash('resultsFormHasError', TRUE);
			$f3->reroute('@quiz');
		}
		\App::db()->exec('INSERT IGNORE INTO results
			SET firstname=:fn, lastname=:ln, datebirth=:dtb, gender=:g, email=:em, password=:p, quiz_data=:qd
			ON DUPLICATE KEY UPDATE firstname=:fn, lastname=:ln, datebirth=:dtb, gender=:g, password=:p, quiz_data=:qd
			', array(
			'fn' => $firstname,
			'ln' => $lastname,
			'dtb' => $datebirth->format('Y-m-d'),
			'g' => $gender,
			'em' => $email,
			'p' => $password,
			'qd' => $quizData,
		));

		$f3->reroute('@thanks');
	}

	private function _getQuizData() {
		$f3 = \Base::instance();
		$quizData = $f3->get('COOKIE.quiz_data');
		$quizDataDecoded = json_decode($quizData, TRUE);
		if (!$quizDataDecoded or count($quizDataDecoded) < count($this->_questions)) {
			$f3->reroute('@quiz');
		}
	}

}
