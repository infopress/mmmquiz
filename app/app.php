<?php

class App {

	const DUMP_FILE = 'data/tables-actual.sql';

	private static $_db = array();

	public static function db($name = 'DB') {
		if (empty(self::$_db[$name])) {
			$confDb = F3::get($name);
			if (empty($confDb['dbname'])) { // Simple check
				F3::error(500, sprintf('Error config db: "%s"', $name));
			}
			self::$_db[$name] = new DB\SQL(sprintf('mysql:host=%s;dbname=%s', $confDb['dsn'], $confDb['dbname'])
					, $confDb['username'], $confDb['password']
			);
			//
			if ($name == 'DB' AND F3::get('DUMP_DB_ACTUAL')) {
				if ($dump = self::_dump($confDb['username'], $confDb['password'], $confDb['dsn'], $confDb['dbname'], FALSE)) {
					F3::write(self::DUMP_FILE, $dump);
				}
			}
		}
		return self::$_db[$name];
	}

	public static function flash($name = NULL, $value = NULL) {
		$f3 = \Base::instance();
		$data = (array) $f3->get('SESSION.flash');
		if (!strlen($name)) {
			$f3->clear('SESSION.flash');
			return $data;
		}
		$name = str_replace('.', '_', $name);
		if ($value === NULL) {
			$f3->clear('SESSION.flash.' . $name);
			return array_key_exists($name, $data) ? $data[$name] : NULL;
		}
		return $f3->set('SESSION.flash', array_merge($data, array($name => $value)));
	}

	public static function response(Array $response = array(), $layout = 'layout.php', $return = FALSE) {
		$f3 = \Base::instance();
		foreach ($response as $key => $val) {
			$f3->set($key, $val);
		}
		// Render HTML layout
		$source = \View::instance()->render($layout);
		if ($return) {
			return $source;
		}
		die($source);
	}

	public static function jsonResponse($data = array(), $options = 0) {
		header('Content-Type: application/json');
		die(json_encode($data, $options));
	}

	public static function jsonError($code, $text = '', array $trace = NULL, $level = 0) {
		$f3 = \Base::instance();
		$header = $f3->status($code);
		$res = array(
			'error' => TRUE,
			'status' => $header,
			'code' => $code,
			'text' => $text ?: 'HTTP ' . $code,
			'level' => $level,
		);
		if ($f3->get('DEBUG')) {
			$res['trace'] = $f3->trace($trace);
		}
		\App::jsonResponse($res);
	}

	private static function _dump($username = 'root', $password = '', $host = 'localhost', $dbname = NULL, $withData = TRUE) {
		$arg = $withData ? '' : '--no-data --compact';
		$dbn = $dbname ?: '--all-databases';
		exec(sprintf('mysqldump --host=%s --user=%s --password=%s %s %s'
						, $host, $username, $password, $arg, $dbn), $arr, $out);
		if ($out === 0) {
			$res = implode("\n", $arr);
			if ($dbname) {
				$res = preg_replace('/AUTO_INCREMENT=\d++\s/', ''
						, str_replace('CREATE TABLE `', 'CREATE TABLE IF NOT EXISTS `', $res));
			}
			return implode("\n", array(
				'SET AUTOCOMMIT=0;',
				'SET FOREIGN_KEY_CHECKS=0;',
				$res,
				'SET FOREIGN_KEY_CHECKS=1;',
				'COMMIT;',
				'SET AUTOCOMMIT=1;',
			));
		}
	}

}
