<?php

require 'lib/vendor/autoload.php';
$f3 = \Base::instance();

$f3->config('app/config.ini');
$f3->config('app/config.env.ini');

// workaround to show debug errors
if ($f3->get('DEBUG') > 1) {
	error_reporting(E_ALL | E_STRICT);
}

// quiz
$f3->route('GET|HEAD @quiz: /', '\Controller\Quiz->index');
$f3->route('GET|HEAD /page-@page', '\Controller\Quiz->page');
$f3->route('GET|HEAD @profile: /quiz/getprofile', '\Controller\Profile->index');
$f3->route('POST /profile', '\Controller\Profile->exec');
// Results
$f3->route('GET|HEAD @results: /results', '\Controller\Results->index');
$f3->route('GET|HEAD @thanks: /thanks', '\Controller\Results->thanks');

$f3->run();
