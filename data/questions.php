<?php

return array(
    array(
        'id' => 1,
        'page_type' => 1,
        'title' => 'My goal is to...',
        'questions' => array(
            array(
                'img' => array('icon01.svg', 'icon01-s.svg'),
                'text' => 'Feel  more  motivated  and  energized',
            ),
            array(
                'img' => array('icon02.svg', 'icon02-s.svg'),
                'text' => 'Feel more comfortable in my own skin',
            ),
            array(
                'img' => array('icon03.svg', 'icon03-s.svg'),
                'text' => 'Feel more calm throughout my day',
            ),
        ),
    ),
    array(
        'id' => 2,
        'page_type' => 1,
        'title' => 'I want to...',
        'questions' => array(
            array(
                'img' => array('icon04.svg', 'icon04-s.svg'),
                'text' => 'Feel more enthusiastic about things I once enjoyed',
            ),
            array(
                'img' => array('icon05.svg', 'icon05-s.svg'),
                'text' => 'Have more confidence around other people',
            ),
            array(
                'img' => array('icon06.svg', 'icon06-s.svg'),
                'text' => 'Feel more focused and peaceful',
            ),
        ),
    ),
    array(
        'id' => 3,
        'page_type' => 1,
        'title' => 'I want to...',
        'questions' => array(
            array(
                'img' => array('icon07.svg', 'icon07-s.svg'),
                'text' => 'Feel in control of depression',
            ),
            array(
                'img' => array('icon08.svg', 'icon08-s.svg'),
                'text' => 'Overcome social anxiety',
            ),
            array(
                'img' => array('icon09.svg', 'icon09-s.svg'),
                'text' => 'Find relief from anxiety and stress',
            ),
        ),
    ),

    array('id' => 4, 'page_type' => 2,
        'title' => 'Feeling fear of judgment in social situations makes it harder to get along with other people.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 5, 'page_type' => 2,
        'title' => 'I feel more nervous and anxious than usual.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 6, 'page_type' => 2,
        'title' => 'I feel afraid for no reason at all.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 7, 'page_type' => 2,
        'title' => 'I get upset easily or feel panicky.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 8, 'page_type' => 2,
        'title' => 'I feel like I’m falling apart and going to pieces.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 9, 'page_type' => 2,
        'title' => 'I feel that everything is all right and nothing bad will happen.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 10, 'page_type' => 2,
        'title' => 'My arms and legs shake and tremble.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 11, 'page_type' => 2,
        'title' => 'I am bothered by headaches neck and back pain.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),
    array('id' => 12, 'page_type' => 2,
        'title' => 'I feel weak and get tired easily.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),

    array('id' => 13, 'page_type' => 2,
        'title' => 'I feel calm and can sit still easily.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),

    array('id' => 14, 'page_type' => 2,
        'title' => 'I can feel my heart beating fast.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),

    array('id' => 15, 'page_type' => 2,
        'title' => 'I am bothered by dizzy spells.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),

    array('id' => 16, 'page_type' => 2,
        'title' => 'I have fainting spells or feel like it.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),

    array('id' => 17, 'page_type' => 2,
        'title' => 'I can breathe in and out easily.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),

    array('id' => 18, 'page_type' => 2,
        'title' => 'I get numbness and tingling in my fingers and toes.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),


    array('id' => 19, 'page_type' => 2,
        'title' => 'I am bothered by stomach aches or indigestion.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),


    array('id' => 20, 'page_type' => 2,
        'title' => 'I have to empty my bladder often.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),


    array('id' => 21, 'page_type' => 2,
        'title' => 'My hands are usually dry and warm.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),


    array('id' => 22, 'page_type' => 2,
        'title' => 'My face gets hot and blushes.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),


    array('id' => 23, 'page_type' => 2,
        'title' => 'I fall asleep easily and get a good night’s rest.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),


    array('id' => 24, 'page_type' => 2,
        'title' => 'I have nightmares.',
        'questions' => array(['text' => 'Not at all'], ['text' => 'A little bit'], ['text' => 'Very much'], ['text' => 'Extremely'])),


);
